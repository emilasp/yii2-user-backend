Модуль Users (BACKEND) для Yii2
=============================

Модуль пользователей для админки

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-user-backend": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-user-backend.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'user' => []
    ],
```
