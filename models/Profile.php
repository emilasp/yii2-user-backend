<?php

namespace emilasp\user\backend\models;

use Yii;

/**
 * This is the model class for table "users_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $hash
 * @property string $name
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property double $rate
 * @property string $data_json
 * @property integer $city_id
 * @property string $hometown
 * @property integer $gender
 * @property string $photo
 * @property string $url
 *
 * @property User $user
 */
class Profile extends \emilasp\user\models\Profile
{

}
