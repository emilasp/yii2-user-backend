<?php

namespace emilasp\user\backend\models;

use Yii;

/**
 * This is the model class for table "users_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $phone
 * @property integer $role
 * @property string $password
 * @property string $auth_key
 * @property string $status
 * @property integer $city_id
 * @property string $created_at
 * @property string $updates_at
 */
class User extends \emilasp\user\models\User
{

}
