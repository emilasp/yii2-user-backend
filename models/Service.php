<?php

namespace emilasp\user\backend\models;

use Yii;

/**
 * This is the model class for table "users_service".
 *
 * @property integer $id
 * @property string $service
 * @property string $ids
 * @property integer $user_id
 * @property string $token
 * @property string $link
 */
class Service extends \emilasp\user\models\Service
{

}
