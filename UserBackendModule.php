<?php
namespace emilasp\user\backend;

use emilasp\core\CoreModule;

/**
 * Class UserBackendModule
 * @package emilasp\user\backend
 */
class UserBackendModule extends CoreModule
{
    public $controllerNamespace = 'emilasp\user\backend\controllers';

    public function init()
    {
        parent::init();
    }


}
