<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\user\backend\models\Service */

$this->title = Yii::t('userbackend', 'Update {modelClass}: ', [
    'modelClass' => 'Service',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('userbackend', 'Services'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('userbackend', 'Update');
?>
<div class="service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
